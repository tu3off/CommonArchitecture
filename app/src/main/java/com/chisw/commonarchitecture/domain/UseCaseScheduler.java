package com.chisw.commonarchitecture.domain;

import android.support.annotation.NonNull;

public interface UseCaseScheduler {
    void execute(@NonNull Runnable command);
}
