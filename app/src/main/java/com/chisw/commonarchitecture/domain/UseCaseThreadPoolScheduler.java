package com.chisw.commonarchitecture.domain;

import android.support.annotation.NonNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UseCaseThreadPoolScheduler implements UseCaseScheduler {
    private ExecutorService executorService;

    public UseCaseThreadPoolScheduler() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final int threadPool = availableProcessors < 2 ? 4 : availableProcessors;
        executorService = Executors.newFixedThreadPool(threadPool);
    }

    @Override
    public void execute(@NonNull Runnable command) {
        executorService.execute(command);
    }
}
