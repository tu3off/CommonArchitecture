package com.chisw.commonarchitecture.domain.usecase;

import android.support.annotation.NonNull;

import com.chisw.commonarchitecture.mvp.model.api.LocalRepository;
import com.chisw.commonarchitecture.mvp.model.api.RemoteRepository;

public abstract class UseCase<Q extends UseCase.QueryValues, R extends UseCase.ResponseValues,
        E extends UseCase.ErrorValues> {
    private final LocalRepository localRepository;
    private final RemoteRepository remoteRepository;
    private Callback<R, E> callback;
    private Q queryValues;

    public UseCase(@NonNull LocalRepository localRepository,
                   @NonNull RemoteRepository remoteRepository) {
        this.localRepository = localRepository;
        this.remoteRepository = remoteRepository;
    }

    public abstract void executeUseCase();

    public Callback<R, E> getCallback() {
        return callback;
    }

    public void setCallback(Callback<R, E> callback) {
        this.callback = callback;
    }

    public Q getQueryValues() {
        return queryValues;
    }

    public void setQueryValues(Q queryValues) {
        this.queryValues = queryValues;
    }

    public LocalRepository getLocalRepository() {
        return localRepository;
    }

    public RemoteRepository getRemoteRepository() {
        return remoteRepository;
    }

    public interface QueryValues {
    }

    public interface ResponseValues {
    }

    public interface ErrorValues {
    }

    public interface Callback<R extends ResponseValues, E extends ErrorValues> {
        void onUseCaseResponse(R response);

        void onUseCaseError(E error);
    }
}
