package com.chisw.commonarchitecture.domain;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.chisw.commonarchitecture.core.Injection;
import com.chisw.commonarchitecture.domain.usecase.UseCase;

public class UseCaseExecutor {
    private static volatile UseCaseExecutor instance;

    @NonNull
    public static UseCaseExecutor getInstance() {
        if (instance == null) {
            synchronized (UseCaseExecutor.class) {
                if (instance == null) {
                    instance = new UseCaseExecutor(Injection.provideUseCaseScheduler());
                }
            }
        }
        return instance;
    }

    private final UseCaseScheduler useCaseScheduler;
    private final Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    private UseCaseExecutor(UseCaseScheduler useCaseScheduler) {
        this.useCaseScheduler = useCaseScheduler;
    }

    public <Q extends UseCase.QueryValues, R extends UseCase.ResponseValues,
            E extends UseCase.ErrorValues> void execute(@NonNull final UseCase<Q, R, E> useCase,
                                                        @NonNull Q queryValues,
                                                        @NonNull UseCase.Callback<R, E> callback) {
        useCase.setQueryValues(queryValues);
        useCase.setCallback(new UiThreadCallbackWrapper<R, E>(callback, mainThreadHandler));
        useCaseScheduler.execute(new Runnable() {
            @Override
            public void run() {
                useCase.executeUseCase();
            }
        });
    }

    private static class UiThreadCallbackWrapper<R extends UseCase.ResponseValues,
            E extends UseCase.ErrorValues> implements UseCase.Callback<R, E> {
        private final UseCase.Callback<R, E> callback;
        private final Handler handler;

        UiThreadCallbackWrapper(@NonNull UseCase.Callback<R, E> callback,
                                @NonNull Handler handler) {
            this.callback = callback;
            this.handler = handler;
        }

        @Override
        public void onUseCaseResponse(final R response) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    callback.onUseCaseResponse(response);
                }
            });
        }

        @Override
        public void onUseCaseError(final E error) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    callback.onUseCaseError(error);
                }
            });
        }
    }
}
