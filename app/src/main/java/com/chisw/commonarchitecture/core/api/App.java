package com.chisw.commonarchitecture.core.api;

public interface App {
    DataSource getDataSource();
}
