package com.chisw.commonarchitecture.core;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.chisw.commonarchitecture.core.api.App;
import com.chisw.commonarchitecture.core.api.DataSource;

public class CApplication extends Application implements App {
    public static App getApp(@NonNull Context context) {
        return (App) context.getApplicationContext();
    }

    private DataSource dataSource;

    @Override
    public void onCreate() {
        super.onCreate();
        dataSource = Injection.provideDataSource(this);
    }

    @Override
    public DataSource getDataSource() {
        return dataSource;
    }
}
