package com.chisw.commonarchitecture.core;

import android.content.Context;
import android.support.annotation.NonNull;

import com.chisw.commonarchitecture.core.api.DataSource;
import com.chisw.commonarchitecture.domain.UseCaseScheduler;
import com.chisw.commonarchitecture.domain.UseCaseThreadPoolScheduler;
import com.chisw.commonarchitecture.mvp.contract.MainContract;
import com.chisw.commonarchitecture.mvp.model.DataSourceRepository;
import com.chisw.commonarchitecture.mvp.model.local.LocalRepositoryManager;
import com.chisw.commonarchitecture.mvp.model.remote.RemoteRepositoryManager;
import com.chisw.commonarchitecture.mvp.presenter.MainPresenter;

public class Injection {
    @NonNull
    public static DataSource provideDataSource(@NonNull Context context) {
        return DataSourceRepository.getInstance(LocalRepositoryManager.getInstance(context),
                RemoteRepositoryManager.getInstance(context));
    }

    @NonNull
    public static UseCaseScheduler provideUseCaseScheduler() {
        return new UseCaseThreadPoolScheduler();
    }

    @NonNull
    public static MainContract.MainPresenter provideMainPresenter() {
        return new MainPresenter();
    }
}
