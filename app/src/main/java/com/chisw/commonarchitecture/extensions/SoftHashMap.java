package com.chisw.commonarchitecture.extensions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SoftHashMap<K, V> extends AbstractMap<K, V> {
    private final ReferenceQueue<V> queue = new ReferenceQueue<>();
    private final Map<K, SoftValue<V>> map;

    public SoftHashMap() {
        map = new HashMap<>();
    }

    @SuppressWarnings("unchecked")
    private void processQueue() {
        while (true) {
            Reference<? extends V> reference = queue.poll();
            if (reference == null) {
                return;
            }
            SoftValue<V> k = (SoftValue<V>) reference;
            Object key = k.key;
            map.remove(key);
        }
    }

    public V get(Object key) {
        processQueue();
        SoftReference<V> o = map.get(key);
        if (o == null) {
            return null;
        }
        return o.get();
    }

    /**
     * Store the object. The return value of this method is null or a SoftReference.
     *
     * @param key the key
     * @param value the value
     * @return null or the old object.
     */
    public V put(K key, V value) {
        processQueue();
        SoftValue<V> old = map.put(key, new SoftValue<V>(value, queue, key));
        return old == null ? null : old.get();
    }

    /**
     * Remove an object.
     *
     * @param key the key
     * @return null or the old object
     */
    @Override
    @Nullable
    public V remove(Object key) {
        processQueue();
        SoftReference<V> ref = map.remove(key);
        return ref == null ? null : ref.get();
    }

    @Override
    public void clear() {
        processQueue();
        map.clear();
    }

    @Override
    @NonNull
    public Set<Entry<K, V>> entrySet() {
        throw new UnsupportedOperationException();
    }

    /**
     * A soft reference that has a hard reference to the key.
     */
    private static class SoftValue<T> extends SoftReference<T> {
        final Object key;

        SoftValue(T ref, ReferenceQueue<T> q, Object key) {
            super(ref, q);
            this.key = key;
        }
    }
}
