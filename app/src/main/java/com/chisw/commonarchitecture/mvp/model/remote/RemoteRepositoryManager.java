package com.chisw.commonarchitecture.mvp.model.remote;

import android.content.Context;
import android.support.annotation.NonNull;

import com.chisw.commonarchitecture.mvp.model.api.RemoteRepository;

public class RemoteRepositoryManager implements RemoteRepository {
    private static volatile RemoteRepositoryManager instance;

    @NonNull
    public static RemoteRepository getInstance(@NonNull Context context) {
        if(instance == null) {
            synchronized (RemoteRepositoryManager.class) {
                if(instance == null) {
                    instance = new RemoteRepositoryManager(context);
                }
            }
        }
        return instance;
    }

    private RemoteRepositoryManager(@NonNull Context context) {
    }
}
