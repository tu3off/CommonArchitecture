package com.chisw.commonarchitecture.mvp.model;

import android.support.annotation.NonNull;

import com.chisw.commonarchitecture.core.api.DataSource;
import com.chisw.commonarchitecture.mvp.model.api.LocalRepository;
import com.chisw.commonarchitecture.mvp.model.api.RemoteRepository;

public class DataSourceRepository implements DataSource {
    private static volatile DataSourceRepository instance;

    @NonNull
    public static DataSourceRepository getInstance(@NonNull LocalRepository localRepository,
                                                   @NonNull RemoteRepository remoteRepository) {
        if (instance == null) {
            synchronized (DataSourceRepository.class) {
                if (instance == null) {
                    instance = new DataSourceRepository(localRepository, remoteRepository);
                }
            }
        }
        return instance;
    }

    private final LocalRepository localRepository;
    private final RemoteRepository remoteRepository;

    private DataSourceRepository(LocalRepository localRepository,
                                 RemoteRepository remoteRepository) {
        this.localRepository = localRepository;
        this.remoteRepository = remoteRepository;
    }
}
