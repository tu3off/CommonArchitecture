package com.chisw.commonarchitecture.mvp.contract;

import com.chisw.commonarchitecture.mvp.presenter.BasePresenter;
import com.chisw.commonarchitecture.mvp.view.BaseView;

public class MainContract {
    public interface MainView extends BaseView {

    }

    public interface MainPresenter extends BasePresenter<MainView> {
    }
}
