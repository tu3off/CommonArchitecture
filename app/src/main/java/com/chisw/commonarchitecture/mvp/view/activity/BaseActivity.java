package com.chisw.commonarchitecture.mvp.view.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.chisw.commonarchitecture.mvp.presenter.BasePresenter;
import com.chisw.commonarchitecture.mvp.presenter.PresenterFactory;
import com.chisw.commonarchitecture.mvp.presenter.PresenterManager;
import com.chisw.commonarchitecture.mvp.view.BaseView;

import java.util.UUID;

public abstract class BaseActivity<V extends BaseView, P extends BasePresenter<V>>
        extends AppCompatActivity implements PresenterFactory<P> {
    private static final String BUNDLE_IDENTIFIER = "bundleIdentifier";
    private String identifier;
    private P presenter;

    protected abstract V obtainView();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            identifier = savedInstanceState.getString(BUNDLE_IDENTIFIER);
        } else {
            identifier = createIdentifier();
        }
        presenter = PresenterManager.getInstance().getPresenter(identifier, this);
        presenter.attachView(obtainView());
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(BUNDLE_IDENTIFIER, identifier);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void finish() {
        presenter = null;
        PresenterManager.getInstance().remove(identifier);
        super.finish();
    }

    protected BasePresenter getPresenter() {
        return presenter;
    }

    @NonNull
    protected String getIdentifier() {
        return identifier;
    }

    @NonNull
    protected String createIdentifier() {
        if (TextUtils.isEmpty(identifier)) {
            return UUID.randomUUID().toString();
        } else {
            return identifier;
        }
    }
}
