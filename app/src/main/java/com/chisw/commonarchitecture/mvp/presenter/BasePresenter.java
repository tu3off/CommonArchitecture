package com.chisw.commonarchitecture.mvp.presenter;

import android.support.annotation.NonNull;

import com.chisw.commonarchitecture.mvp.view.BaseView;

public interface BasePresenter<V extends BaseView> {
    void attachView(@NonNull V view);

    void detachView();
}
