package com.chisw.commonarchitecture.mvp.presenter;

public interface PresenterFactory<P extends BasePresenter> {
    P createPresenter();
}
