package com.chisw.commonarchitecture.mvp.presenter;

import android.util.Log;

import com.chisw.commonarchitecture.extensions.SoftHashMap;
import com.chisw.commonarchitecture.mvp.view.BaseView;

import java.util.Map;

public class PresenterManager {
    private static volatile PresenterManager instance;

    public static PresenterManager getInstance() {
        if(instance == null) {
            synchronized (PresenterManager.class) {
                if(instance == null) {
                    instance = new PresenterManager();
                }
            }
        }
        return instance;
    }

    private final Map<String, BasePresenter> presenterCache = new SoftHashMap<>();

    private PresenterManager() {
    }

    @SuppressWarnings("unchecked")
    public final <V extends BaseView, P extends BasePresenter<V>>
    P getPresenter(String key, PresenterFactory<P> factory) {
        P presenter = null;
        try {
            presenter = (P) presenterCache.get(key);
        } catch (ClassCastException exception) {
            Log.w("PresenterManager", "Duplicate presenter with key: " + key);
        }
        if(presenter == null) {
            presenter = factory.createPresenter();
            presenterCache.put(key, presenter);
        }
        return presenter;
    }

    public void remove(String key) {
        presenterCache.remove(key);
    }
}
