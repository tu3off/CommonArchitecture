package com.chisw.commonarchitecture.mvp.model.local;

import android.content.Context;
import android.support.annotation.NonNull;

import com.chisw.commonarchitecture.mvp.model.api.LocalRepository;

public class LocalRepositoryManager implements LocalRepository {
    private static volatile LocalRepositoryManager instance;

    @NonNull
    public static LocalRepository getInstance(@NonNull Context context) {
        if(instance == null) {
            synchronized (LocalRepositoryManager.class) {
                if(instance == null) {
                    instance = new LocalRepositoryManager(context);
                }
            }
        }
        return instance;
    }

    private LocalRepositoryManager(Context context) {
    }
}
