package com.chisw.commonarchitecture.mvp.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import java.util.UUID;

public abstract class BaseFragment extends Fragment {
    private static final String BUNDLE_IDENTIFIER = "bundleIdentifier";
    private String identifier;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null) {
            identifier = savedInstanceState.getString(BUNDLE_IDENTIFIER);
        } else {
            identifier = createIdentifier();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(BUNDLE_IDENTIFIER, identifier);
        super.onSaveInstanceState(outState);
    }

    @NonNull
    protected String getIdentifier() {
        return identifier;
    }

    @NonNull
    protected String createIdentifier() {
        if(TextUtils.isEmpty(identifier)) {
            return UUID.randomUUID().toString();
        } else {
            return identifier;
        }
    }
}
