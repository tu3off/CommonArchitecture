package com.chisw.commonarchitecture.mvp.presenter;

import android.support.annotation.NonNull;

import com.chisw.commonarchitecture.mvp.contract.MainContract;

public class MainPresenter implements MainContract.MainPresenter{
    @Override
    public void attachView(@NonNull MainContract.MainView view) {

    }

    @Override
    public void detachView() {

    }
}
