package com.chisw.commonarchitecture.mvp.view.activity;

import com.chisw.commonarchitecture.core.Injection;
import com.chisw.commonarchitecture.mvp.contract.MainContract;

public class MainActivity extends BaseActivity<MainContract.MainView, MainContract.MainPresenter>
        implements MainContract.MainView {
    @Override
    public MainContract.MainPresenter createPresenter() {
        return Injection.provideMainPresenter();
    }

    @Override
    protected MainContract.MainView obtainView() {
        return this;
    }
}
